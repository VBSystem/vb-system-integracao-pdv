program DemoIntegrador;

uses
  Vcl.Forms,
  UPrincipal in 'UPrincipal.pas' {Form1},
  vbsystem.integracao.pdv in '..\vbsystem.integracao.pdv.pas',
  base.model in '..\models\base.model.pas',
  cliente.model in '..\models\cliente.model.pas',
  contador.model in '..\models\contador.model.pas',
  dav.model in '..\models\dav.model.pas',
  empresa.model in '..\models\empresa.model.pas',
  finalizadora.model in '..\models\finalizadora.model.pas',
  prevenda.model in '..\models\prevenda.model.pas',
  produto.model in '..\models\produto.model.pas',
  reducaoz.model in '..\models\reducaoz.model.pas',
  unidade.model in '..\models\unidade.model.pas',
  usuario.model in '..\models\usuario.model.pas',
  venda.model in '..\models\venda.model.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
