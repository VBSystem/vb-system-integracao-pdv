unit UPrincipal;

interface

uses
  vbsystem.integracao.pdv,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    ListBox1: TListBox;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button7: TButton;
    Button8: TButton;
    Button9: TButton;
    Button10: TButton;
    Button11: TButton;
    Button6: TButton;
    Button12: TButton;
    Button13: TButton;
    Button14: TButton;
    Button15: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure Button15Click(Sender: TObject);
  private
    FIntegracao: TIntegracaoPDV;
  public

  end;

var
  Form1: TForm1;

implementation

uses
  System.Math,
  System.Generics.Collections,

  venda.model,
  reducaoz.model, synacode, System.DateUtils;

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  FIntegracao := TIntegracaoPDV.Create('http://localhost', 8080);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FIntegracao.Free;
end;

procedure TForm1.Button10Click(Sender: TObject);
begin
  FIntegracao.AbrirDia(0.00);
end;

procedure TForm1.Button11Click(Sender: TObject);
begin
  FIntegracao.FecharDia;
end;

procedure TForm1.Button12Click(Sender: TObject);
var
  Venda: TVenda;
  Vendas: TObjectList<TVenda>;
  IdVenda: string;
begin
  IdVenda := InputBox('Venda', 'Digite o ID da Venda:', '');
  if IdVenda.Trim.IsEmpty then
    raise Exception.Create('Id da Venda n�o foi informado.');


  Vendas := FIntegracao.BuscarVendaID(IdVenda);
  if Assigned(Vendas) then
  begin
    try
      ListBox1.Items.BeginUpdate;
      try
        for Venda in Vendas do
        begin
          ListBox1.Items.Add(
            Format('%s - %d - %.2f (%s)', [
              DateToStr(Venda.ECF.DtEmissao),
              Venda.ECF.Ccf_cupom,
              Venda.ValorTotal,
              DecodeBase64(Venda.NFCe.XML)
            ])
          );
        end;
      finally
        ListBox1.ItemIndex := 0;
        ListBox1.Items.EndUpdate;
      end;
    finally
      Vendas.DisposeOf;
    end;
  end;
end;

procedure TForm1.Button13Click(Sender: TObject);
var
  Venda: TVenda;
  Vendas: TObjectList<TVenda>;
  DataInicial, DataFinal: TDateTime;
begin
  DataInicial := StrToDate(InputBox('Data inicial', 'Data (dd/mm/yyyy): ', DateToStr(DATE)));
  DataFinal   := StrToDate(InputBox('Data inicial', 'Data (dd/mm/yyyy): ', DateToStr(DATE)));

  Vendas := FIntegracao.BuscarVendaPeriodo(DataInicial, DataFinal);
  if Assigned(Vendas) then
  begin
    try
      ListBox1.Items.BeginUpdate;
      try
        for Venda in Vendas do
        begin
          ListBox1.Items.Add(
            Format('%s - %d - %.2f (%s)', [
              DateToStr(Venda.ECF.DtEmissao),
              Venda.ECF.Ccf_cupom,
              Venda.ValorTotal,
              DecodeBase64(Venda.NFCe.XML)
            ])
          );
        end;
      finally
        ListBox1.ItemIndex := 0;
        ListBox1.Items.EndUpdate;
      end;
    finally
      Vendas.DisposeOf;
    end;
  end;
end;

procedure TForm1.Button14Click(Sender: TObject);
var
  oECF: TVendaECF;
begin
  oECF := FIntegracao.DadosDoECFConectado;
  if not Assigned(oECF) then
    raise Exception.Create('N�o foi poss�vel obter dados do ECF conectado');

  ListBox1.Items.BeginUpdate;
  try
    ListBox1.Items.Add('N�mero: '             + oECF.Sequencial.ToString);
    ListBox1.Items.Add('N�mero de S�rie: '    + oECF.Numero_serie);
    ListBox1.Items.Add('Numero CRZ: '         + oECF.NumCrz.ToString);
    ListBox1.Items.Add('Numero CRO: '         + oECF.NumCro.ToString);
    ListBox1.Items.Add('N�mero COO: '         + oECF.Coo_cupom.ToString);
    ListBox1.Items.Add('N�mero CCF: '         + oECF.Ccf_cupom.ToString);
    ListBox1.Items.Add('Marca: '              + oECF.Marca);
    ListBox1.Items.Add('Modelo: '             + oECF.Modelo);
    ListBox1.Items.Add('Firmware: '           + oECF.Versao_firmware);
    ListBox1.Items.Add('Data de Instala��o: ' + DateTimeToStr(oECF.Dthr_instalacao));
    ListBox1.Items.Add('Data de Movimento: '  + DateTimeToStr(oECF.DtFiscal));
  finally
    ListBox1.Items.EndUpdate;
  end;
end;

procedure TForm1.Button15Click(Sender: TObject);
var
  Aliquotas: TAliquotasPadrao;
begin
  Aliquotas := TAliquotasPadrao.Create;
  try
    with Aliquotas.New do
    begin
      valor := 17.00;
      Tipo  := 'T';
    end;

    with Aliquotas.New do
    begin
      valor := 18.00;
      Tipo  := 'T';
    end;

    with Aliquotas.New do
    begin
      valor := 19.00;
      Tipo  := 'T';
    end;

    if FIntegracao.CadastrarAliquotas(Aliquotas) then
      ShowMessage('Aliquotas cadastradas com sucesso!');
  finally
    Aliquotas.Free;
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  Venda: TVenda;
  VendaItem: TVendaItem;
  VendaPgto: TVendaPagamento;
begin
  Randomize;

  Venda := TVenda.Create;
  try
    // cabecalho
    Venda.IDVenda := Random.ToString;
    Venda.Cliente.NOME     := 'teste pdv';
    Venda.Cliente.CPF_CNPJ := '849.151.706-59';

    //item
    VendaItem := Venda.Itens.New;
    VendaItem.VendaQuantidade        := 1;
    VendaItem.VendaDesconto          := 0;
    VendaItem.VendaAcrescimo         := 0;
    VendaItem.ID_PRODUTO             := 1;
    VendaItem.ATIVO                  := 'S';
    VendaItem.DESCRICAO              := 'produto teste api 1';
    VendaItem.DEPARTAMENTO           := 1;
    VendaItem.DESCRICAO_DEPARTAMENTO := 'geral';
    VendaItem.SECAO                  := 1;
    VendaItem.DESCRICAO_SECAO        := 'geral';
    VendaItem.UNIDADE                := 'UN';
    VendaItem.QT_ESTOQUE             := 10;
    VendaItem.VL_UNITARIO            := 1.23;
    VendaItem.ALIQUOTA_TIPO          := 'T';
    VendaItem.ALIQUOTA_VALOR         := 18.00;
    VendaItem.TIPO_PRODUCAO          := 'P';
    VendaItem.TIPO_ARREDONDAMENTO    := 'A';

    VendaItem := Venda.Itens.New;
    VendaItem.VendaQuantidade        := 1;
    VendaItem.VendaDesconto          := 0;
    VendaItem.VendaAcrescimo         := 0;
    VendaItem.ID_PRODUTO             := 2;
    VendaItem.ATIVO                  := 'S';
    VendaItem.DESCRICAO              := 'produto teste api 2';
    VendaItem.DEPARTAMENTO           := 1;
    VendaItem.DESCRICAO_DEPARTAMENTO := 'geral';
    VendaItem.SECAO                  := 1;
    VendaItem.DESCRICAO_SECAO        := 'geral';
    VendaItem.UNIDADE                := 'UN';
    VendaItem.QT_ESTOQUE             := 10;
    VendaItem.VL_UNITARIO            := 1.23;
    VendaItem.ALIQUOTA_TIPO          := 'II';
    VendaItem.ALIQUOTA_VALOR         := 0.00;
    VendaItem.TIPO_PRODUCAO          := 'P';
    VendaItem.TIPO_ARREDONDAMENTO    := 'A';


    VendaPgto := Venda.Pagamentos.New;
    VendaPgto.TIPO      := 1;
    VendaPgto.DESCRICAO := 'dinheiro';
    VendaPgto.ValorPago := 10.00;

    if FIntegracao.EnviarVenda(Venda) then
      ShowMessage(Venda.ECF.Ccf_cupom.ToString);
  finally
    Venda.DisposeOf
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  ReducaoZ: TReducaoZ;
  ReducoesZ: TObjectList<TReducaoZ>;
begin
  ReducoesZ := FIntegracao.BuscarReducaoZ(StrToDate('01/01/2000'), StrToDate('31/12/2020'));
  if Assigned(ReducoesZ) then
  begin
    try
      ListBox1.Items.BeginUpdate;
      try
        for ReducaoZ in ReducoesZ do
        begin
          ListBox1.Items.Add(
            Format('%s - %d - %.2f', [
              DateToStr(ReducaoZ.DT_EMISSAO),
              ReducaoZ.CRZ,
              ReducaoZ.VL_VENDALIQUIDA
            ])
          );
        end;
      finally
        ListBox1.ItemIndex := 0;
        ListBox1.Items.EndUpdate;
      end;
    finally
      ReducoesZ.DisposeOf;
    end;
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  // informar numero do ecf e ccf somente quando for nfce ou sat/mfe
  if FIntegracao.CancelarVenda(0, 0) then
    ShowMessage('venda cancelada!');

  // pode-se utlizar tamb�m
  // FIntegracao.CancelarVenda('id da prevenda');
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  if FIntegracao.IsServidorOnLine then
    ShowMessage('Servidor on-line!');
end;

procedure TForm1.Button5Click(Sender: TObject);
var
  StatusPDV: TStatusVenda;
begin
  StatusPDV := FIntegracao.StatusPDV;
  try
    ShowMessage(Format('%d, %s', [StatusPDV.Codigo, StatusPDV.Mensagem]));
  finally
    StatusPDV.DisposeOf;
  end;
end;

procedure TForm1.Button6Click(Sender: TObject);
var
  ChaveDigitada: string;
begin
  ChaveDigitada := InputBox('Chave Acesso', 'Digite a chave de acesso:', '');
  if ChaveDigitada.IsEmpty then
    raise Exception.Create('Digite uma chave de acesso do nfc-e ou cf-e para continuar.');

  ShowMessage(FIntegracao.BuscarXML(ChaveDigitada));
end;

procedure TForm1.Button7Click(Sender: TObject);
begin
  ShowMessage(FIntegracao.UltimoCCF);
end;

procedure TForm1.Button8Click(Sender: TObject);
begin
  ShowMessage(FIntegracao.UltimoCOO);
end;

procedure TForm1.Button9Click(Sender: TObject);
begin
  ShowMessage(FIntegracao.NumeroPDV);
end;

end.
