object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Demo'
  ClientHeight = 816
  ClientWidth = 802
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 30
    Top = 524
    Width = 201
    Height = 36
    Caption = 'Enviar Venda'
    TabOrder = 10
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 30
    Top = 676
    Width = 201
    Height = 36
    Caption = 'Retornar Redu'#231#245'es Z'
    TabOrder = 13
    OnClick = Button2Click
  end
  object ListBox1: TListBox
    Left = 260
    Top = 25
    Width = 511
    Height = 771
    ItemHeight = 13
    TabOrder = 9
  end
  object Button3: TButton
    Left = 30
    Top = 566
    Width = 201
    Height = 36
    Caption = 'Cancelar venda'
    TabOrder = 11
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 30
    Top = 25
    Width = 201
    Height = 36
    Caption = 'On-Line?'
    TabOrder = 0
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 30
    Top = 67
    Width = 201
    Height = 36
    Caption = 'Status PDV'
    TabOrder = 1
    OnClick = Button5Click
  end
  object Button7: TButton
    Left = 30
    Top = 260
    Width = 201
    Height = 36
    Caption = 'CCF'
    TabOrder = 5
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 30
    Top = 302
    Width = 201
    Height = 36
    Caption = 'COO'
    TabOrder = 6
    OnClick = Button8Click
  end
  object Button9: TButton
    Left = 30
    Top = 344
    Width = 201
    Height = 36
    Caption = 'N'#250'mero PDV'
    TabOrder = 7
    OnClick = Button9Click
  end
  object Button10: TButton
    Left = 30
    Top = 176
    Width = 201
    Height = 36
    Caption = 'Abrir o Dia'
    TabOrder = 3
    OnClick = Button10Click
  end
  object Button11: TButton
    Left = 30
    Top = 218
    Width = 201
    Height = 36
    Caption = 'Fechar o Dia'
    TabOrder = 4
    OnClick = Button11Click
  end
  object Button6: TButton
    Left = 30
    Top = 608
    Width = 201
    Height = 36
    Caption = 'Retornar XML NFC-e/CF-e'
    TabOrder = 12
    OnClick = Button6Click
  end
  object Button12: TButton
    Left = 30
    Top = 718
    Width = 201
    Height = 36
    Caption = 'Retornar Venda Por ID'
    TabOrder = 14
    OnClick = Button12Click
  end
  object Button13: TButton
    Left = 30
    Top = 760
    Width = 201
    Height = 36
    Caption = 'Retornar Venda Por Per'#237'odo'
    TabOrder = 15
    OnClick = Button13Click
  end
  object Button14: TButton
    Left = 30
    Top = 109
    Width = 201
    Height = 36
    Caption = 'Dados do ECF'
    TabOrder = 2
    OnClick = Button14Click
  end
  object Button15: TButton
    Left = 30
    Top = 386
    Width = 201
    Height = 36
    Caption = 'Cadastrar Al'#237'quotas Padr'#227'o'
    TabOrder = 8
    OnClick = Button15Click
  end
end
