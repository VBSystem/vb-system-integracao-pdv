unit dav.model;

interface

uses
  base.model,
  System.SysUtils,
  System.Classes,
  System.Generics.Collections,
  MVCFramework.Swagger.Commons,
  MVCFramework.Serializer.Commons;

type
  TDAV = class;

  [MVCNameCaseAttribute(ncLowerCase)]
  TDAVItem = class
  private
    FOwner: TDAV;
    FID_PRODUTO: Integer;
    FALIQUOTA_TIPO: Integer;
    FCANCELADO: string;
    FALIQUOTA_VALOR: Double;
    FALIQUOTA_INDICE: Integer;
    FDT_INCLUSAO: TDate;
    FUNIDADE: string;
    FQUANTIDADE: Double;
    FID_ITEM: Integer;
    FVL_ACRESCIMO: Double;
    FVL_DESCONTO: Double;
    FVL_UNITARIO: Double;
    function GetID_DAV: Integer;
  public
    constructor Create(const AOwner: TDAV);

    [MVCDoNotSerializeAttribute]
    property ID_DAV: Integer read GetID_DAV;

    property ID_ITEM: Integer read FID_ITEM write FID_ITEM;
    property ID_PRODUTO: Integer read FID_PRODUTO write FID_PRODUTO;
    property DT_INCLUSAO: TDate read FDT_INCLUSAO write FDT_INCLUSAO;
    property QUANTIDADE: Double read FQUANTIDADE write FQUANTIDADE;
    property UNIDADE: string read FUNIDADE write FUNIDADE;
    property VL_UNITARIO: Double read FVL_UNITARIO write FVL_UNITARIO;
    property VL_DESCONTO: Double read FVL_DESCONTO write FVL_DESCONTO;
    property VL_ACRESCIMO: Double read FVL_ACRESCIMO write FVL_ACRESCIMO;
    property ALIQUOTA_INDICE: Integer read FALIQUOTA_INDICE write FALIQUOTA_INDICE;

    [MVCSwagJsonSchemaField('', 'Tipo de Al�quota do Produto (T,S,II,NN,FF,SI,SN,SF)')]
    property ALIQUOTA_TIPO: Integer read FALIQUOTA_TIPO write FALIQUOTA_TIPO;
    property ALIQUOTA_VALOR: Double read FALIQUOTA_VALOR write FALIQUOTA_VALOR;

    [MVCSwagJsonSchemaField('', 'Produto Cancelado? (S Sim, N N�o)')]
    property CANCELADO: string read FCANCELADO write FCANCELADO;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TDAV = class(TBaseModel)
  private
    FItens: TObjectList<TDAVItem>;
    FVL_TOTAL: Double;
    FDT_FECHAMENTO: TDate;
    FCRO: Integer;
    FID_ECF: Integer;
    FCOO_CUPOM: Integer;
    FDT_EXCLUSAO: TDate;
    FID_CLIENTE: Integer;
    FID_VENDEDOR: Integer;
    FSITUACAO: string;
    FID_DAV: Integer;
    FDT_CADASTRO: Integer;
    FVL_ACRESCIMO: Double;
    FVL_DESCONTO: Double;
    FCCF_CUPOM: Integer;
    FIMPRESSO: string;
    FOBSERVACOES: string;
    FCOO_GERENCIAL: Integer;
  public
    constructor Create;
    destructor Destroy; override;

    property ID_DAV: Integer read FID_DAV write FID_DAV;

    [MVCSwagJsonSchemaField('', 'Situa��o do DAV (A Aberto, F Fechado)')]
    property SITUACAO: string read FSITUACAO write FSITUACAO;

    [MVCSwagJsonSchemaField('', 'DAV j� foi impresso? (S Sim, N N�o)')]
    property IMPRESSO: string read FIMPRESSO write FIMPRESSO;
    property VL_DESCONTO: Double read FVL_DESCONTO write FVL_DESCONTO;
    property VL_ACRESCIMO: Double read FVL_ACRESCIMO write FVL_ACRESCIMO;
    property DT_CADASTRO: Integer read FDT_CADASTRO write FDT_CADASTRO;
    property ID_CLIENTE: Integer read FID_CLIENTE write FID_CLIENTE;
    property VL_TOTAL: Double read FVL_TOTAL write FVL_TOTAL;
    property DT_FECHAMENTO: TDate read FDT_FECHAMENTO write FDT_FECHAMENTO;
    property DT_EXCLUSAO: TDate read FDT_EXCLUSAO write FDT_EXCLUSAO;
    property ID_VENDEDOR: Integer read FID_VENDEDOR write FID_VENDEDOR;
    property ID_ECF: Integer read FID_ECF write FID_ECF;
    property COO_GERENCIAL: Integer read FCOO_GERENCIAL write FCOO_GERENCIAL;
    property COO_CUPOM: Integer read FCOO_CUPOM write FCOO_CUPOM;
    property CCF_CUPOM: Integer read FCCF_CUPOM write FCCF_CUPOM;
    property CRO: Integer read FCRO write FCRO;
    property OBSERVACOES: string read FOBSERVACOES write FOBSERVACOES;

    property Itens: TObjectList<TDAVItem> read FItens write FItens;
  end;

implementation

uses
  ACBrUtil;

{ TDAV }

constructor TDAV.Create;
begin
  inherited Create;
  FItens := TObjectList<TDAVItem>.Create;
end;

destructor TDAV.Destroy;
begin
  FItens.DisposeOf;
  inherited;
end;

{ TDAVItem }

constructor TDAVItem.Create(const AOwner: TDAV);
begin
  inherited Create;
  FOwner := AOwner;
end;

function TDAVItem.GetID_DAV: Integer;
begin
  Result := FOwner.ID_DAV;
end;

end.
