unit usuario.model;

interface

uses
  base.model,

  System.SysUtils,
  System.Classes,
  System.Generics.Collections,

  MVCFramework.Swagger.Commons,
  MVCFramework.Serializer.Commons;

type
  [MVCNameCaseAttribute(ncLowerCase)]
  TUsuario = class(TBaseModel)
  private
    FATIVO: string;
    FSENHA: string;
    FNOME: string;
    FTIPO: string;
    FID_USUARIO: integer;
  public
    property ID_USUARIO: integer read FID_USUARIO write FID_USUARIO;

    [MVCSwagJsonSchemaField('', 'Tipo de usu�rio? (S Supervisor, O Operador, V Vendedor)')]
    property TIPO: string read FTIPO write FTIPO;
    property NOME: string read FNOME write FNOME;
    property SENHA: string read FSENHA write FSENHA;

    [MVCSwagJsonSchemaField('', 'Usu�rio ativo? (S Sim ou N N�o)')]
    property ATIVO: string read FATIVO write FATIVO;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TUsuarios = class(TObjectList<TUsuario>)
  private

  public
    function New: TUsuario;
  end;

implementation

uses
  ACBrUtil;

{ TUsuarios }

function TUsuarios.New: TUsuario;
begin
  Result := TUsuario.Create;
  Self.Add(Result);
end;

end.

