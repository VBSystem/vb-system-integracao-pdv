unit venda.model;

interface

uses
  base.model,
  produto.model,
  finalizadora.model,
  cliente.model,

  System.SysUtils,
  System.Classes,
  System.Generics.Collections,

  MVCFramework.Swagger.Commons,
  MVCFramework.Serializer.Commons;

type
  [MVCNameCaseAttribute(ncLowerCase)]
  TAliquotaPadrao = class
  private
    FValor: Double;
    FTipo: String;
  public
    property Valor: Double read FValor write FValor;

    [MVCSwagJsonSchemaField('', 'Tipo de Al�quota (T - Tributada ou S - Servi�o)')]
    property Tipo: String read FTipo write FTipo;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TAliquotasPadrao = class(TObjectList<TAliquotaPadrao>)
  public
    function New: TAliquotaPadrao;
  end;


  [MVCNameCaseAttribute(ncLowerCase)]
  TStatusVenda = class(TBaseModel)
  private
    FCodigo: Integer;
    FMensagem: string;
  public
    property Codigo: Integer read FCodigo write FCodigo;
    property Mensagem: string read FMensagem write FMensagem;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TVendaNFCe = class(TBaseModel)
  private
    FcStat: Integer;
    FProtocolo: string;
    FDtHrProtocolo: TDateTime;
    FSerie: Integer;
    FNumero: Integer;
    FxMotivo: string;
    FXML: WideString;
    FOffLine: Boolean;
    FNumeroSerieSAT: string;
    FChave: string;
  public
    property Serie: Integer read FSerie write FSerie;
    property Numero: Integer read FNumero write FNumero;
    property OffLine: Boolean read FOffLine write FOffLine;
    property Protocolo: string read FProtocolo write FProtocolo;
    property DtHrProtocolo: TDateTime read FDtHrProtocolo write FDtHrProtocolo;
    property cStat: Integer read FcStat write FcStat;
    property xMotivo: string read FxMotivo write FxMotivo;
    property XML: WideString read FXML write FXML;
    property NumeroSerieSAT: string read FNumeroSerieSAT write FNumeroSerieSAT;
    property Chave: string read FChave write FChave;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TVendaECF = class(TBaseModel)
  private
    FDtFiscal: TDateTime;
    FSequencial: Integer;
    FDtFinal: TDateTime;
    FCniee: String;
    FNumCro: Integer;
    FCoo_cupom: Integer;
    FModelo: String;
    FVersao_firmware: String;
    FNumero_serie: String;
    FNumero_proprietarios: Integer;
    FNumCrz: Integer;
    FCcf_cupom: Integer;
    FMarca: String;
    FDtEmissao: TDateTime;
    FMf_adicional: String;
    FDthr_instalacao: tdateTime;
    FEmitirZAutomatico: Boolean;
  public
    property Sequencial: Integer read FSequencial write FSequencial;
    property Numero_serie: String read FNumero_serie write FNumero_serie;
    property NumCrz: Integer read FNumCrz write FNumCrz;
    property NumCro: Integer read FNumCro write FNumCro;
    property Marca: String read FMarca write FMarca;
    property Modelo: String read FModelo write FModelo;
    property Numero_proprietarios: Integer read FNumero_proprietarios write FNumero_proprietarios;
    property MF_adicional: String read FMf_adicional write FMf_adicional;
    property Versao_firmware: String read FVersao_firmware write FVersao_firmware;
    property Dthr_instalacao: tdateTime read FDthr_instalacao write FDthr_instalacao;
    property Cniee: String read FCniee write FCniee;
    property Coo_cupom: Integer read FCoo_cupom write FCoo_cupom;
    property Ccf_cupom: Integer read FCcf_cupom write FCcf_cupom;
    property DtFiscal: TDateTime read FDtFiscal write FDtFiscal;
    property DtEmissao: TDateTime read FDtEmissao write FDtEmissao;
    property DtFinal: TDateTime read FDtFinal write FDtFinal;
    property EmitirZAutomatico: Boolean read FEmitirZAutomatico write FEmitirZAutomatico default False;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TVendaPagamento = class(TFinalizadora)
  private
    FValorPago: Double;
    FObservacao: string;
  public
    property ValorPago: Double read FValorPago write FValorPago;
    property Observacao: string read FObservacao write FObservacao;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TVendaItem = class(TProduto)
  private
    FVendaAcrescimo: Double;
    FVendaDesconto: Double;
    FQuantidadeVendida: Double;
    function GetVendaTotal: Double;
  public
    property VendaQuantidade: Double read FQuantidadeVendida write FQuantidadeVendida;
    property VendaDesconto: Double read FVendaDesconto write FVendaDesconto;
    property VendaAcrescimo: Double read FVendaAcrescimo write FVendaAcrescimo;
    property VendaTotal: Double read GetVendaTotal;
  end;

  TVendaItens = class(TObjectList<TVendaItem>)
  public
    function New: TVendaItem;
  end;

  TVendaPagamentos = class(TObjectList<TVendaPagamento>)
  public
    function New: TVendaPagamento;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TVenda = class(TBaseModel)
  private
    FItens: TVendaItens;
    FPagamentos: TVendaPagamentos;
    FCliente: TCliente;
    FECF: TVendaECF;
    FRodape: string;
    FIDVenda: string;
    FNFCe: TVendaNFCe;
    FCancelado: Boolean;
    FLojaId: string;
    FPosId: string;
    FCaixaId: string;
    FOperador: string;
    FValorTotal: Double;
    FValorSubTotal: Double;
    FValorAcrescimo: Double;
    FValorDesconto: Double;
    FValorPago: Double;
    FValorTroco: Double;
  public
    constructor Create;
    destructor Destroy; override;

    property IDVenda: string read FIDVenda write FIDVenda;
    property Cancelado: Boolean read FCancelado write FCancelado;
    property LojaId: string read FLojaId write FLojaId;
    property PosId: string read FPosId write FPosId;
    property CaixaId: string read FCaixaId write FCaixaId;
    property Operador: string read FOperador write FOperador;
    property ValorSubTotal: Double read FValorSubTotal write FValorSubTotal;
    property ValorAcrescimo: Double read FValorAcrescimo write FValorAcrescimo;
    property ValorDesconto: Double read FValorDesconto write FValorDesconto;
    property ValorTotal: Double read FValorTotal write FValorTotal;
    property ValorPago: Double read FValorPago write FValorPago;
    property ValorTroco: Double read FValorTroco write FValorTroco;
    property Cliente: TCliente read FCliente write FCliente;
    property Itens: TVendaItens read FItens write FItens;
    property Pagamentos: TVendaPagamentos read FPagamentos write FPagamentos;
    property Rodape: string read FRodape write FRodape;
    property ECF: TVendaECF read FECF write FECF;
    property NFCe: TVendaNFCe read FNFCe write FNFCe;
  end;

  TVendas = class(TObjectList<TVenda>)
  public
    function New: TVenda;
  end;

implementation

uses
  ACBrUtil;

{ TVenda }

constructor TVenda.Create;
begin
  inherited Create;
  FCliente := TCliente.Create;
  FECF := TVendaECF.Create;
  FNFCe := TVendaNFCe.Create;
  FItens := TVendaItens.Create;
  FPagamentos := TVendaPagamentos.Create;
end;

destructor TVenda.Destroy;
begin
  FCliente.DisposeOf;
  FECF.DisposeOf;
  FNFCe.DisposeOf;
  FItens.DisposeOf;
  FPagamentos.DisposeOf;
  inherited;
end;

{ TVendaItem }

function TVendaItem.GetVendaTotal: Double;
begin
  Result := (VendaQuantidade * VL_UNITARIO) - VendaDesconto + VendaAcrescimo;
end;

{ TVendaItens }

function TVendaItens.New: TVendaItem;
begin
  Result := TVendaItem.Create;
  Self.Add(Result);
end;

{ TVendaPagamentos }

function TVendaPagamentos.New: TVendaPagamento;
begin
  Result := TVendaPagamento.Create;
  Self.Add(Result);
end;

{ TVendas }

function TVendas.New: TVenda;
begin
  Result := TVenda.Create;
  Self.Add(Result);
end;

{ TAliquotasPadrao }

function TAliquotasPadrao.New: TAliquotaPadrao;
begin
  Result := TAliquotaPadrao.Create;
  Self.Add(Result);
end;

end.
