unit finalizadora.model;

interface

uses
  base.model,

  System.SysUtils,
  System.Classes,
  System.Generics.Collections,

  MVCFramework.Swagger.Commons,
  MVCFramework.Serializer.Commons;

type
  [MVCNameCaseAttribute(ncLowerCase)]
  TFinalizadora = class(TBaseModel)
  private
    FID_PGTO_NFCE: Integer;
    FSOLICITAR_SUPERVISOR: string;
    FSOLICITAR_CLIENTE: string;
    FDESCRICAO: string;
    FID_FINALIZADORA: Integer;
    FPERMITIR_TROCO: string;
    FSOLICITAR_TEF: string;
    FVL_MAXIMO_TROCO: Double;
    FABRIR_GAVETA: string;
    FID_PGTO_SAT: Integer;
    FTIPO: Integer;
  public
    property ID_FINALIZADORA : Integer    read FID_FINALIZADORA      write FID_FINALIZADORA;

    [MVCSwagJsonSchemaField('', 'Tipo de Finalizadora (1 Dinheiro, 2 Cheque, 3 Cart�o, 4 Ticket, 5 Credi�rio, 6 Conv�nio, 7 POS)')]
    property TIPO: Integer                read FTIPO                 write FTIPO;
    property DESCRICAO: string            read FDESCRICAO            write FDESCRICAO;

    [MVCSwagJsonSchemaField('', 'Abre a gaveta? (S Sim ou N N�o)')]
    property ABRIR_GAVETA: string         read FABRIR_GAVETA         write FABRIR_GAVETA;

    [MVCSwagJsonSchemaField('', 'Solicita senha de supervisor? (S Sim ou N N�o)')]
    property SOLICITAR_SUPERVISOR: string read FSOLICITAR_SUPERVISOR write FSOLICITAR_SUPERVISOR;

    [MVCSwagJsonSchemaField('', 'Finalizadora chama o TEF? (S Sim ou N N�o)')]
    property SOLICITAR_TEF: string        read FSOLICITAR_TEF        write FSOLICITAR_TEF;

    [MVCSwagJsonSchemaField('', 'Obriga informar Cliente? (S Sim ou N N�o)')]
    property SOLICITAR_CLIENTE: string    read FSOLICITAR_CLIENTE    write FSOLICITAR_CLIENTE;

    [MVCSwagJsonSchemaField('', 'Permite Troco? (S Sim ou N N�o)')]
    property PERMITIR_TROCO: string       read FPERMITIR_TROCO       write FPERMITIR_TROCO;

    property VL_MAXIMO_TROCO: Double      read FVL_MAXIMO_TROCO      write FVL_MAXIMO_TROCO;

    [MVCSwagJsonSchemaField('', 'C�digo da forma de pagamento conforme tabela de tipos no manual da NF-e')]
    property ID_PGTO_NFCE: Integer        read FID_PGTO_NFCE         write FID_PGTO_NFCE;

    [MVCSwagJsonSchemaField('', 'C�digo da forma de pagamento conforme tabela de tipos no manual do SAT')]
    property ID_PGTO_SAT: Integer         read FID_PGTO_SAT          write FID_PGTO_SAT;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TFinalizadoras = class(TObjectList<TFinalizadora>)
  private

  public
    function New: TFinalizadora;
  end;

implementation

uses
  ACBrUtil;

{ TFinalizadoras }

function TFinalizadoras.New: TFinalizadora;
begin
  Result := TFinalizadora.Create;
  Self.Add(Result);
end;

end.

