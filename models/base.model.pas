unit base.model;

interface

type
  TResultModel = class
  private
    FResult: string;
  public
    property Result: string read FResult write FResult;
  end;

  TFundoCaixaModel = class
  private
    FValor: Double;
  public
    property Valor: Double read FValor write FValor;
  end;

  TBaseModel = class
  private

  public
    function AsJsonString: WideString;
  end;

implementation

uses
  JsonDataObjects,
  MVCFramework.Serializer.Commons,
  MVCFramework.Serializer.JsonDataObjects;

{ TBaseModel }

function TBaseModel.AsJsonString: WideString;
var
  Serializar: TMVCJsonDataObjectsSerializer;
  JsonObj: TJSONObject;
begin
  Serializar := TMVCJsonDataObjectsSerializer.Create;
  JsonObj    := TJSONObject.Create;
  try
    Serializar.ObjectToJSONObject(Self, JsonObj, stDefault, []);
    Result := JsonObj.ToJSON;
  finally
    Serializar.Free;
    JsonObj.Free;
  end;
end;

end.

