unit cliente.model;

interface

uses
  base.model,

  System.SysUtils,
  System.Classes,
  System.Generics.Collections,

  MVCFramework.Swagger.Commons,
  MVCFramework.Serializer.Commons;

type
  [MVCNameCaseAttribute(ncLowerCase)]
  TCliente = class(TBaseModel)
  private
    FCPF_CNPJ: string;
    FBAIRRO: string;
    FRG_IE: string;
    FUF: string;
    FVL_LIMITE: Double;
    FID_CLIENTE: Integer;
    FCEP: string;
    FNUMERO: string;
    FTIPO_PESSOA: string;
    FSITUACAO: string;
    FCOMPLEMENTO: string;
    FNOME: string;
    FCIDADE: string;
    FENDERECO: string;
    FTELEFONE: string;
    FDT_ALTERACAO: TDateTime;
    function GetCEP: string;
    function GetCPF_CNPJ: string;
    function GetRG_IE: string;
    function GetTELEFONE: string;
    function GetEnderecoCompleto: string;
  public
    constructor Create;

    property ID_CLIENTE: Integer     read FID_CLIENTE   write FID_CLIENTE;

    [MVCSwagJsonSchemaField('', 'Situa��o do Cadastro (L Liberado, B Bloqueado)')]
    property SITUACAO: string        read FSITUACAO     write FSITUACAO;
    property NOME: string            read FNOME         write FNOME;
    property VL_LIMITE: Double       read FVL_LIMITE    write FVL_LIMITE;

    [MVCSwagJsonSchemaField('', 'Tipo de Pessoa (F F�sica ou J Jur�dica)')]
    property TIPO_PESSOA: string     read FTIPO_PESSOA  write FTIPO_PESSOA;
    property CPF_CNPJ: string        read GetCPF_CNPJ   write FCPF_CNPJ;
    property RG_IE: string           read GetRG_IE      write FRG_IE;
    property TELEFONE: string        read GetTELEFONE   write FTELEFONE;
    property ENDERECO: string        read FENDERECO     write FENDERECO;
    property NUMERO: string          read FNUMERO       write FNUMERO;
    property COMPLEMENTO: string     read FCOMPLEMENTO  write FCOMPLEMENTO;
    property BAIRRO: string          read FBAIRRO       write FBAIRRO;
    property CIDADE: string          read FCIDADE       write FCIDADE;
    property UF: string              read FUF           write FUF;
    property CEP: string             read GetCEP        write FCEP;
    property DT_ALTERACAO: TDateTime read FDT_ALTERACAO write FDT_ALTERACAO;

    [MVCDoNotSerializeAttribute]
    property EnderecoCompleto: string read GetEnderecoCompleto;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TClientes = class(TObjectList<TCliente>)
  private

  public
    function New: TCliente;
  end;

implementation

uses
  ACBrUtil;

{ TClientes }

function TClientes.New: TCliente;
begin
  Result := TCliente.Create;
  Self.Add(Result);
end;

{ TCliente }

constructor TCliente.Create;
begin
  inherited Create;
  FDT_ALTERACAO := Now;
end;

function TCliente.GetCEP: string;
begin
  Result := OnlyNumber(FCEP);
end;

function TCliente.GetCPF_CNPJ: string;
begin
  Result := OnlyNumber(FCPF_CNPJ);
end;

function TCliente.GetEnderecoCompleto: string;
begin
  Result := Self.ENDERECO + ', ' + Self.NUMERO;
end;

function TCliente.GetRG_IE: string;
begin
  Result := OnlyNumber(FRG_IE);
end;

function TCliente.GetTELEFONE: string;
begin
  Result := OnlyNumber(FTELEFONE);
end;

end.
