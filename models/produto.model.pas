unit produto.model;

interface

uses
  base.model,

  System.SysUtils,
  System.Classes,
  System.Generics.Collections,

  MVCFramework.Swagger.Commons,
  MVCFramework.Serializer.Commons;

type
  TProduto = class;

  [MVCNameCaseAttribute(ncLowerCase)]
  TProdutoReferencia = class(TBaseModel)
  private
    FOwner: TProduto;
    FREFERENCIA: string;
    function GetID_PRODUTO: Integer;
  public
    constructor Create(const AOwner: TProduto);

    [MVCDoNotSerializeAttribute]
    property ID_PRODUTO: Integer read GetID_PRODUTO;

    property REFERENCIA: string read FREFERENCIA write FREFERENCIA;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TProdutoGrade = class(TBaseModel)
  private
    FOwner: TProduto;
    FCOR: string;
    FQT_ENTRADAS: Double;
    FID_GRADE: Integer;
    FQT_ESTOQUE: Double;
    FQT_SAIDAS: Double;
    FTAMANHO: string;
    function GetID_PRODUTO: Integer;
  public
    constructor Create(const AOwner: TProduto);

    [MVCDoNotSerializeAttribute]
    property ID_PRODUTO: Integer read GetID_PRODUTO;

    property ID_GRADE: Integer   read FID_GRADE write FID_GRADE;
    property COR: string         read FCOR write FCOR;
    property TAMANHO: string     read FTAMANHO write FTAMANHO;
    property QT_ESTOQUE: Double  read FQT_ESTOQUE write FQT_ESTOQUE;
    property QT_ENTRADAS: Double read FQT_ENTRADAS write FQT_ENTRADAS;
    property QT_SAIDAS: Double   read FQT_SAIDAS write FQT_SAIDAS;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TProdutoEan = class(TBaseModel)
  private
    FOwner: TProduto;
    FMULTIPLICADOR: Integer;
    FUNIDADE: string;
    FEAN: string;
    function GetID_PRODUTO: Integer;
  public
    constructor Create(const AOwner: TProduto);


    [MVCDoNotSerializeAttribute]
    property ID_PRODUTO: Integer    read GetID_PRODUTO;

    property EAN: string            read FEAN           write FEAN;
    property UNIDADE: string        read FUNIDADE       write FUNIDADE;
    property MULTIPLICADOR: Integer read FMULTIPLICADOR write FMULTIPLICADOR;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TProduto = class(TBaseModel)
  private
    FEans: TObjectList<TProdutoEan>;
    FID_PRODUTO: Integer;
    FPIS_CST: string;
    FALIQUOTA_TIPO: string;
    FCOFINS_CST: string;
    FTIPO_PRODUCAO: string;
    FIMPRIME_PIZZARIA: string;
    FATIVO: string;
    FDESCRICAO: string;
    FCEST: string;
    FIMPRIME_COZINHA: string;
    FALIQUOTA_VALOR: Double;
    FDT_ALTERACAO: TDateTime;
    FCFOP: Integer;
    FIMPRIME_COPA: string;
    FLOCALIZACAO: string;
    FNCM: string;
    FPIS_ALIQ: Double;
    FDESCRICAO_DEPARTAMENTO: string;
    FCOFINS_ALIQ: Double;
    FDEPARTAMENTO: Integer;
    FBALANCA: string;
    FUNIDADE: string;
    FPIS_BC: Double;
    FCOFINS_BC: Double;
    FALIQ_APROX_IMPOSTOS: Double;
    FUTILIZA_3DECIMAIS: string;
    FQT_ESTOQUE: Double;
    FTIPO_ARREDONDAMENTO: string;
    FDT_VENC_PROMOCAO: TDate;
    FREFERENCIA: string;
    FPESADO: string;
    FDESCRICAO_SECAO: string;
    FVL_UNITARIO: Double;
    FPRODUTO_COMPOSTO: string;
    FSECAO: Integer;
    FORIGEM: Integer;
    FCST: string;
    FCSOSN: string;
    FGrades: TObjectList<TProdutoGrade>;
    FReferencias: TObjectList<TProdutoReferencia>;
    function GetAliquotaTexto: string;
  public
    constructor Create;
    destructor Destroy; override;

    property ID_PRODUTO: Integer            read FID_PRODUTO             write FID_PRODUTO;

    [MVCSwagJsonSchemaField('', 'Produto ativo? (S Sim ou N N�o)')]
    property ATIVO: string                  read FATIVO                  write FATIVO;
    property DESCRICAO: string              read FDESCRICAO              write FDESCRICAO;
    property DEPARTAMENTO: Integer          read FDEPARTAMENTO           write FDEPARTAMENTO;
    property DESCRICAO_DEPARTAMENTO: string read FDESCRICAO_DEPARTAMENTO write FDESCRICAO_DEPARTAMENTO;
    property SECAO: Integer                 read FSECAO                  write FSECAO;
    property DESCRICAO_SECAO: string        read FDESCRICAO_SECAO        write FDESCRICAO_SECAO;
    property UNIDADE: string                read FUNIDADE                write FUNIDADE;
    property VL_UNITARIO: Double            read FVL_UNITARIO            write FVL_UNITARIO;
    property QT_ESTOQUE: Double             read FQT_ESTOQUE             write FQT_ESTOQUE;

    [MVCSwagJsonSchemaField('', 'Tipo da al�quota do produto (T Tributado, S Servi�o, II Isento, NN N�o Tributado, FF Substitui��o, SI,SN,SF')]
    property ALIQUOTA_TIPO: string          read FALIQUOTA_TIPO          write FALIQUOTA_TIPO;
    property ALIQUOTA_VALOR: Double         read FALIQUOTA_VALOR         write FALIQUOTA_VALOR;

    [MVCDoNotSerializeAttribute]
    property AliquotaTexto: string read GetAliquotaTexto;

    [MVCSwagJsonSchemaField('', 'Tipo de Produ��o do produto (P ou T)')]
    property TIPO_PRODUCAO: string          read FTIPO_PRODUCAO          write FTIPO_PRODUCAO;

    [MVCSwagJsonSchemaField('', 'Tipo de Arredondamento (A ou T)')]
    property TIPO_ARREDONDAMENTO: string    read FTIPO_ARREDONDAMENTO    write FTIPO_ARREDONDAMENTO;

    [MVCSwagJsonSchemaField('', 'Produto de Balan�a? (S Sim ou N N�o)')]
    property BALANCA: string                read FBALANCA                write FBALANCA;

    [MVCSwagJsonSchemaField('', 'Produto Pesado? (S Sim ou N N�o)')]
    property PESADO: string                 read FPESADO                 write FPESADO;

    [MVCSwagJsonSchemaField('', 'Produto utiliza 3 decimais na venda? (S Sim ou N N�o)')]
    property UTILIZA_3DECIMAIS: string      read FUTILIZA_3DECIMAIS      write FUTILIZA_3DECIMAIS;

    [MVCSwagJsonSchemaField('', 'Produto Composto? (S Sim ou N N�o)')]
    property PRODUTO_COMPOSTO: string       read FPRODUTO_COMPOSTO       write FPRODUTO_COMPOSTO;
    property CST: string                    read FCST                    write FCST;
    property NCM: string                    read FNCM                    write FNCM;
    property LOCALIZACAO: string            read FLOCALIZACAO            write FLOCALIZACAO;

    [MVCSwagJsonSchemaField('', 'Informar o c�digo de identifica��o do produto no ERP')]
    property REFERENCIA: string             read FREFERENCIA             write FREFERENCIA;
    property DT_VENC_PROMOCAO: TDate        read FDT_VENC_PROMOCAO       write FDT_VENC_PROMOCAO;
    property DT_ALTERACAO: TDateTime        read FDT_ALTERACAO           write FDT_ALTERACAO;
    property ALIQ_APROX_IMPOSTOS: Double    read FALIQ_APROX_IMPOSTOS    write FALIQ_APROX_IMPOSTOS;

    [MVCSwagJsonSchemaField('', 'Produto imprime na cozinha? (S Sim ou N N�o)')]
    property IMPRIME_COZINHA: string        read FIMPRIME_COZINHA        write FIMPRIME_COZINHA;

    [MVCSwagJsonSchemaField('', 'Produto imprime na copa? (S Sim ou N N�o)')]
    property IMPRIME_COPA: string           read FIMPRIME_COPA           write FIMPRIME_COPA;

    [MVCSwagJsonSchemaField('', 'Produto pizzaria? (S Sim ou N N�o)')]
    property IMPRIME_PIZZARIA: string       read FIMPRIME_PIZZARIA       write FIMPRIME_PIZZARIA;
    property PIS_CST: string                read FPIS_CST                write FPIS_CST;
    property PIS_BC: Double                 read FPIS_BC                 write FPIS_BC;
    property PIS_ALIQ: Double               read FPIS_ALIQ               write FPIS_ALIQ;
    property COFINS_CST: string             read FCOFINS_CST             write FCOFINS_CST;
    property COFINS_BC: Double              read FCOFINS_BC              write FCOFINS_BC;
    property COFINS_ALIQ: Double            read FCOFINS_ALIQ            write FCOFINS_ALIQ;
    property ORIGEM: Integer                read FORIGEM                 write FORIGEM;
    property CFOP: Integer                  read FCFOP                   write FCFOP;
    property CSOSN: string                  read FCSOSN                  write FCSOSN;
    property CEST: string                   read FCEST                   write FCEST;

    property Eans: TObjectList<TProdutoEan> read FEans write FEans;
    property Grades: TObjectList<TProdutoGrade> read FGrades write FGrades;
    property Referencias: TObjectList<TProdutoReferencia> read FReferencias write FReferencias;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TProdutos = class(TObjectList<TProduto>)
  private

  public
    function New: TProduto;
  end;

implementation

uses
  ACBrUtil, System.StrUtils;

{ TProduto }

constructor TProduto.Create;
begin
  inherited Create;

  FID_PRODUTO             := 0;
  FATIVO                  := 'S';
  FDESCRICAO              := '';
  FDEPARTAMENTO           := 1;
  FDESCRICAO_DEPARTAMENTO := 'GERAL';
  FSECAO                  := 1;
  FDESCRICAO_SECAO        := 'GERAL';
  FUNIDADE                := 'UN';
  FVL_UNITARIO            := 0.00;
  FQT_ESTOQUE             := 0.00;
  FALIQUOTA_TIPO          := 'I';
  FALIQUOTA_VALOR         := 0.00;
  FTIPO_PRODUCAO          := 'T';
  FTIPO_ARREDONDAMENTO    := 'A';
  FBALANCA                := 'N';
  FPESADO                 := 'N';
  FUTILIZA_3DECIMAIS      := 'N';
  FPRODUTO_COMPOSTO       := 'N';
  FCST                    := '040';
  FNCM                    := '';
  FLOCALIZACAO            := '';
  FREFERENCIA             := '';
  FDT_VENC_PROMOCAO       := 0.0;
  FDT_ALTERACAO           := NOW;
  FALIQ_APROX_IMPOSTOS    := 0.00;
  FIMPRIME_COZINHA        := 'N';
  FIMPRIME_COPA           := 'N';
  FIMPRIME_PIZZARIA       := 'N';
  FPIS_CST                := '07';
  FPIS_BC                 := 0.00;
  FPIS_ALIQ               := 0.00;
  FCOFINS_CST             := '07';
  FCOFINS_BC              := 0.00;
  FCOFINS_ALIQ            := 0.00;
  FORIGEM                 := 0;
  FCFOP                   := 5102;
  FCSOSN                  := '101';
  FCEST                   := '';

  FEans := TObjectList<TProdutoEan>.Create;
  FGrades := TObjectList<TProdutoGrade>.Create;
  FReferencias := TObjectList<TProdutoReferencia>.Create;
end;

destructor TProduto.Destroy;
begin
  FEans.DisposeOf;
  FGrades.DisposeOf;
  FReferencias.DisposeOf;
  inherited;
end;

function TProduto.GetAliquotaTexto: string;
begin
  case ALIQUOTA_TIPO[1] of
    'T': Result := ReplaceStr(Format('%.2fT', [ALIQUOTA_VALOR]), ',', '.');
    'S': Result := ReplaceStr(Format('%.2fS', [ALIQUOTA_VALOR]), ',', '.');
    'F': REsult := 'FF';
    'N': REsult := 'NN';
    'I': REsult := 'II';
  end;
end;

{ TProdutoEan }

constructor TProdutoEan.Create(const AOwner: TProduto);
begin
  inherited Create;
  FOwner := AOwner;
end;

function TProdutoEan.GetID_PRODUTO: Integer;
begin
  Result := FOwner.ID_PRODUTO;
end;

{ TProdutoGrade }

constructor TProdutoGrade.Create(const AOwner: TProduto);
begin
  inherited Create;
  FOwner := AOwner;
end;

function TProdutoGrade.GetID_PRODUTO: Integer;
begin
  Result := FOwner.ID_PRODUTO;
end;

{ TProdutoReferencia }

constructor TProdutoReferencia.Create(const AOwner: TProduto);
begin
  inherited Create;
  FOwner := AOwner;
end;

function TProdutoReferencia.GetID_PRODUTO: Integer;
begin
  Result := FOwner.ID_PRODUTO;
end;

{ TProdutos }

function TProdutos.New: TProduto;
begin
  Result := TProduto.Create;
  Self.Add(Result);
end;

end.

