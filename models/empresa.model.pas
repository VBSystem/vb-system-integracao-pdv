unit empresa.model;

interface

uses
  base.model,

  System.SysUtils,
  System.Classes,
  System.Generics.Collections,

  MVCFramework.Swagger.Commons,
  MVCFramework.Serializer.Commons;

type
  [MVCNameCaseAttribute(ncLowerCase)]
  TEmpresa = class(TBaseModel)
  private
    FFONE: string;
    FPERFIL_FISCAL: string;
    FCNPJ: string;
    FEMAIL: string;
    FBAIRRO: string;
    FRAZAO_SOCIAL: string;
    FTIPO_ATIVIDADE: Integer;
    FUF: string;
    FNIRE: Integer;
    FINSCR_MUNICIPAL: string;
    FPROGRAMA_CREDITO_DF: string;
    FCEP: string;
    FNUMERO: string;
    FSUFRAMA: string;
    FNOME_FANTASIA: string;
    FCOMPLEMENTO: string;
    FCONTATO: string;
    FINSCR_ESTADUAL: string;
    FCRT: Integer;
    FCIDADE: string;
    FENDERECO: string;
    function GetCEP: string;
    function GetCNPJ: string;
  public
    constructor Create;

    property CNPJ: string                read GetCNPJ              write FCNPJ;
    property RAZAO_SOCIAL: string        read FRAZAO_SOCIAL        write FRAZAO_SOCIAL;
    property NOME_FANTASIA: string       read FNOME_FANTASIA       write FNOME_FANTASIA;

    [MVCSwagJsonSchemaField('', 'Perfil Fiscal do SPED (A,B,C)')]
    property PERFIL_FISCAL: string       read FPERFIL_FISCAL       write FPERFIL_FISCAL;

    [MVCSwagJsonSchemaField('', 'Tipo de Atividade (0 Industrial, 1 Servi�os, 2 Com�rcio, 3 Financeira, 4 Imobili�ria, 9 Outros)')]
    property TIPO_ATIVIDADE: Integer     read FTIPO_ATIVIDADE      write FTIPO_ATIVIDADE;
    property INSCR_ESTADUAL: string      read FINSCR_ESTADUAL      write FINSCR_ESTADUAL;
    property INSCR_MUNICIPAL: string     read FINSCR_MUNICIPAL     write FINSCR_MUNICIPAL;
    property SUFRAMA: string             read FSUFRAMA             write FSUFRAMA;
    property ENDERECO: string            read FENDERECO            write FENDERECO;
    property NUMERO: string              read FNUMERO              write FNUMERO;
    property COMPLEMENTO: string         read FCOMPLEMENTO         write FCOMPLEMENTO;
    property BAIRRO: string              read FBAIRRO              write FBAIRRO;
    property CIDADE: string              read FCIDADE              write FCIDADE;
    property UF: string                  read FUF                  write FUF;
    property CEP: string                 read GetCEP               write FCEP;
    property CONTATO: string             read FCONTATO             write FCONTATO;
    property FONE: string                read FFONE                write FFONE;
    property EMAIL: string               read FEMAIL               write FEMAIL;

    [MVCSwagJsonSchemaField('', 'Participa do programa de cr�dido do Distrito Federal? (S Sim ou N N�o)')]
    property PROGRAMA_CREDITO_DF: string read FPROGRAMA_CREDITO_DF write FPROGRAMA_CREDITO_DF;
    property NIRE: Integer               read FNIRE                write FNIRE;

    [MVCSwagJsonSchemaField('', 'C�digo do Regime Tributario? (1 Simples Nacional, 2 Simples Nacional com excesso sublimite, 3 Regime Normal)')]
    property CRT: Integer                read FCRT                 write FCRT;
  end;


implementation

uses
  ACBrUtil;

{ TEmpresa }

constructor TEmpresa.Create;
begin
  inherited Create;
  FCRT                 := 0;
  FTIPO_ATIVIDADE      := 0;
  FPERFIL_FISCAL       := 'A';
  FPROGRAMA_CREDITO_DF := 'N';
end;

function TEmpresa.GetCEP: string;
begin
  Result := OnlyNumber(FCEP);
end;

function TEmpresa.GetCNPJ: string;
begin
  Result := OnlyNumber(FCNPJ);
end;

end.

