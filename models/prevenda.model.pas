unit prevenda.model;

interface

uses
  base.model,

  System.SysUtils,
  System.Classes,
  System.Generics.Collections,

  MVCFramework.Swagger.Commons,
  MVCFramework.Serializer.Commons;

type
  TPreVenda = class;

  [MVCNameCaseAttribute(ncLowerCase)]
  TPreVendaItem = class(TBaseModel)
  private
    FOwner: TPreVenda;
    FID_PRODUTO: Integer;
    FCANCELADO: string;
    FQUANTIDADE: Double;
    FID_ITEM: Integer;
    FVL_ACRESCIMO: Double;
    FVL_DESCONTO: Double;
    function GetID_PREVENDA: Integer;
  public
    constructor Create(const AOwner: TPreVenda);

    [MVCDoNotSerializeAttribute]
    property ID_PREVENDA: Integer read GetID_PREVENDA;

    property ID_ITEM: Integer read FID_ITEM write FID_ITEM;
    property ID_PRODUTO: Integer read FID_PRODUTO write FID_PRODUTO;
    property QUANTIDADE: Double read FQUANTIDADE write FQUANTIDADE;
    property VL_DESCONTO: Double read FVL_DESCONTO write FVL_DESCONTO;
    property VL_ACRESCIMO: Double read FVL_ACRESCIMO write FVL_ACRESCIMO;

    [MVCSwagJsonSchemaField('', 'Produto foi Cancelado? (S Sim, N N�o)')]
    property CANCELADO: string read FCANCELADO write FCANCELADO;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TPreVenda = class(TBaseModel)
  private
    FItens: TObjectList<TPreVendaItem>;
    FDT_FECHAMENTO: TDateTime;
    FID_PREVENDA: Integer;
    FID_CLIENTE: Integer;
    FDT_EXCLUSAO: TDateTime;
    FID_VENDEDOR: Integer;
    FSITUACAO: string;
    FDT_CADASTRO: TDateTime;
    FVL_ACRESCIMO: Double;
    FVL_DESCONTO: Double;
  public
    constructor Create;
    destructor Destroy; override;

    property ID_PREVENDA: Integer read FID_PREVENDA write FID_PREVENDA;

    [MVCSwagJsonSchemaField('', 'Situa��o da Pre-Venda (A Aberta, L Liberada, B Bloqueada)')]
    property SITUACAO: string read FSITUACAO write FSITUACAO;
    property VL_DESCONTO: Double read FVL_DESCONTO write FVL_DESCONTO;
    property VL_ACRESCIMO: Double read FVL_ACRESCIMO write FVL_ACRESCIMO;
    property DT_CADASTRO: TDateTime read FDT_CADASTRO write FDT_CADASTRO;
    property DT_FECHAMENTO: TDateTime read FDT_FECHAMENTO write FDT_FECHAMENTO;
    property DT_EXCLUSAO: TDateTime read FDT_EXCLUSAO write FDT_EXCLUSAO;
    property ID_VENDEDOR: Integer read FID_VENDEDOR write FID_VENDEDOR;
    property ID_CLIENTE: Integer read FID_CLIENTE write FID_CLIENTE;

    property Itens: TObjectList<TPreVendaItem> read FItens write FItens;
  end;


implementation

uses
  ACBrUtil;

{ TPreVenda }

constructor TPreVenda.Create;
begin
  inherited Create;
  FItens := TObjectList<TPreVendaItem>.Create;
end;

destructor TPreVenda.Destroy;
begin
  FItens.DisposeOf;
  inherited;
end;

{ TPreVendaItem }

constructor TPreVendaItem.Create(const AOwner: TPreVenda);
begin
  inherited Create;
  FOwner := AOwner;
end;

function TPreVendaItem.GetID_PREVENDA: Integer;
begin
  Result := FOwner.ID_PREVENDA;
end;

end.
