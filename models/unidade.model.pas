unit unidade.model;

interface

uses
  base.model,

  System.SysUtils,
  System.Classes,
  System.Generics.Collections,

  MVCFramework.Swagger.Commons,
  MVCFramework.Serializer.Commons;

type
  [MVCNameCaseAttribute(ncLowerCase)]
  TUnidade = class(TBaseModel)
  private
    FDESCRICAO: string;
    FUNIDADE: string;
  public
    property UNIDADE: string read FUNIDADE write FUNIDADE;
    property DESCRICAO: string read FDESCRICAO write FDESCRICAO;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TUnidades = class(TObjectList<TUnidade>)
  private

  public
    function New: TUnidade;
  end;

implementation

uses
  ACBrUtil;

{ TUnidades }

function TUnidades.New: TUnidade;
begin
  Result := TUnidade.Create;
  Self.Add(Result);
end;

end.

