unit reducaoz.model;

interface

uses
  base.model,
  produto.model,
  finalizadora.model,
  cliente.model,

  System.SysUtils,
  System.Classes,
  System.Generics.Collections,

  MVCFramework.Swagger.Commons,
  MVCFramework.Serializer.Commons;

type
  [MVCNameCaseAttribute(ncLowerCase)]
  TReducaoZItem = class(TBaseModel)
  private
    FVL_ALIQUOTA: Double;
    FINDICE: Integer;
    FVL_BASE: Double;
    FVL_IMPOSTO: Double;
    FTIPO: string;
  public


    [MVCSwagJsonSchemaField('', 'Tipo de Al�quota (T Tributado, S Servi�o)')]
    property TIPO: string read FTIPO write FTIPO;
    property INDICE: Integer read FINDICE write FINDICE;
    property VL_ALIQUOTA: Double read FVL_ALIQUOTA write FVL_ALIQUOTA;
    property VL_BASE: Double read FVL_BASE write FVL_BASE;
    property VL_IMPOSTO: Double read FVL_IMPOSTO write FVL_IMPOSTO;
  end;

  TReducaoZItens = class(TObjectList<TReducaoZItem>)
  public
    function New: TReducaoZItem;
  end;

  [MVCNameCaseAttribute(ncLowerCase)]
  TReducaoZ = class(TBaseModel)
  private
    FAliquotas: TReducaoZItens;
    FCFD: Integer;
    FECF: Integer;
    FVL_OPERACAONAOFISCAL: Double;
    FCOO_FINAL: Integer;
    FCRO: Integer;
    FCOO_INICIAL: Integer;
    FCCDC: Integer;
    FVL_ACRESCIMOISSQN: Double;
    FVL_DESCONTOISSQN: Double;
    FVL_SANGRIA: Double;
    FVL_CANCELAMENTOICMS: Double;
    FNCN: Integer;
    FCRZ: Integer;
    FVL_VENDALIQUIDA: Double;
    FGRG: Integer;
    FCOO: Integer;
    FDT_EMISSAO: TDateTime;
    FVL_SUPRIMENTO: Double;
    FVL_CANCELAMENTOISSQN: Double;
    FNFC: Integer;
    FVL_VENDABRUTA: Double;
    FGNF: Integer;
    FCFC: Integer;
    FCCF: Integer;
    FVL_GTFINAL: Double;
    FVL_GTINICIAL: Double;
    FCDC: Integer;
    FDT_MOVIMENTO: TDate;
    FVL_ACRESCIMOICMS: Double;
    FVL_DESCONTOICMS: Double;
  public
    constructor Create;
    destructor Destroy; override;

    property ECF: Integer                 read FECF                  write FECF;
    property DT_EMISSAO: TDateTime        read FDT_EMISSAO           write FDT_EMISSAO;
    property DT_MOVIMENTO: TDate          read FDT_MOVIMENTO         write FDT_MOVIMENTO;
    property COO: Integer                 read FCOO                  write FCOO;
    property CRZ: Integer                 read FCRZ                  write FCRZ;
    property CRO: Integer                 read FCRO                  write FCRO;
    property GNF: Integer                 read FGNF                  write FGNF;
    property CCF: Integer                 read FCCF                  write FCCF;
    property GRG: Integer                 read FGRG                  write FGRG;
    property CFD: Integer                 read FCFD                  write FCFD;
    property NFC: Integer                 read FNFC                  write FNFC;
    property CFC: Integer                 read FCFC                  write FCFC;
    property CDC: Integer                 read FCDC                  write FCDC;
    property NCN: Integer                 read FNCN                  write FNCN;
    property CCDC: Integer                read FCCDC                 write FCCDC;
    property COO_INICIAL: Integer         read FCOO_INICIAL          write FCOO_INICIAL;
    property COO_FINAL: Integer           read FCOO_FINAL            write FCOO_FINAL;
    property VL_GTINICIAL: Double         read FVL_GTINICIAL         write FVL_GTINICIAL;
    property VL_GTFINAL: Double           read FVL_GTFINAL           write FVL_GTFINAL;
    property VL_VENDABRUTA: Double        read FVL_VENDABRUTA        write FVL_VENDABRUTA;
    property VL_VENDALIQUIDA: Double      read FVL_VENDALIQUIDA      write FVL_VENDALIQUIDA;
    property VL_SANGRIA: Double           read FVL_SANGRIA           write FVL_SANGRIA;
    property VL_SUPRIMENTO: Double        read FVL_SUPRIMENTO        write FVL_SUPRIMENTO;
    property VL_OPERACAONAOFISCAL: Double read FVL_OPERACAONAOFISCAL write FVL_OPERACAONAOFISCAL;
    property VL_CANCELAMENTOICMS: Double  read FVL_CANCELAMENTOICMS  write FVL_CANCELAMENTOICMS;
    property VL_DESCONTOICMS: Double      read FVL_DESCONTOICMS      write FVL_DESCONTOICMS;
    property VL_ACRESCIMOICMS: Double     read FVL_ACRESCIMOICMS     write FVL_ACRESCIMOICMS;
    property VL_CANCELAMENTOISSQN: Double read FVL_CANCELAMENTOISSQN write FVL_CANCELAMENTOISSQN;
    property VL_DESCONTOISSQN: Double     read FVL_DESCONTOISSQN     write FVL_DESCONTOISSQN;
    property VL_ACRESCIMOISSQN: Double    read FVL_ACRESCIMOISSQN    write FVL_ACRESCIMOISSQN;

    property Aliquotas: TReducaoZItens read FAliquotas write FAliquotas;
  end;

  TReducoesZ = class(TObjectList<TReducaoZ>)
  public
    function New: TReducaoZ;
  end;

implementation

{ TReducaoZItens }

function TReducaoZItens.New: TReducaoZItem;
begin
  Result := TReducaoZItem.Create;
  Self.Add(Result);
end;

{ TReducaoZ }

constructor TReducaoZ.Create;
begin
  inherited Create;
  FAliquotas := TReducaoZItens.Create;
end;

destructor TReducaoZ.Destroy;
begin
  FAliquotas.DisposeOf;
  inherited;
end;

{ TReducoesZ }

function TReducoesZ.New: TReducaoZ;
begin
  Result := TReducaoZ.Create;
  Self.Add(Result);
end;

end.
