unit contador.model;

interface

uses
  base.model,

  System.SysUtils,
  System.Classes,
  System.Generics.Collections,

  MVCFramework.Swagger.Commons,
  MVCFramework.Serializer.Commons;

type
  [MVCNameCaseAttribute(ncLowerCase)]
  TContador = class(TBaseModel)
  private
    FFONE: string;
    FCNPJ: string;
    FEMAIL: string;
    FBAIRRO: string;
    FCRC: string;
    FUF: string;
    FCPF: string;
    FCEP: string;
    FNUMERO: string;
    FCOMPLEMENTO: string;
    FNOME: string;
    FCIDADE: string;
    FENDERECO: string;
  public
    property NOME: string        read FNOME        write FNOME;
    property CRC: string         read FCRC         write FCRC;
    property CPF: string         read FCPF         write FCPF;
    property CNPJ: string        read FCNPJ        write FCNPJ;
    property ENDERECO: string    read FENDERECO    write FENDERECO;
    property NUMERO: string      read FNUMERO      write FNUMERO;
    property COMPLEMENTO: string read FCOMPLEMENTO write FCOMPLEMENTO;
    property BAIRRO: string      read FBAIRRO      write FBAIRRO;
    property CIDADE: string      read FCIDADE      write FCIDADE;
    property UF: string          read FUF          write FUF;
    property CEP: string         read FCEP         write FCEP;
    property FONE: string        read FFONE        write FFONE;
    property EMAIL: string       read FEMAIL       write FEMAIL;
  end;

implementation

uses
  ACBrUtil;

end.

