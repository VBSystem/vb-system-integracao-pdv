unit vbsystem.integracao.pdv;

interface

uses
  System.SysUtils,
  System.Variants,
  System.Classes,
  System.Generics.Collections,

  base.model,
  cliente.model,
  contador.model,
  dav.model,
  empresa.model,
  finalizadora.model,
  prevenda.model,
  produto.model,
  reducaoz.model,
  unidade.model,
  usuario.model,
  venda.model,

  MVCFramework.RESTClient,
  MVCFramework.Serializer.Commons,
  MVCFramework.Serializer.Intf;

type
  EIntegracaoPDV = class(Exception);

  TIntegracaoPDV = class
  private
    FURL: string;
    FPorta: Integer;
    FTimeout: Integer;
    FRESTClient: TRESTClient;
    FRESTResponse: IRESTResponse;
    FSerializers: TDictionary<string, IMVCSerializer>;
    procedure CreateComponentes;
    procedure FreeComponentes;
    procedure ShowError(AResponse: IRESTResponse);
    function BodyAs<T: class, constructor>(const AJSON: string): T;
    function BodyAsListOf<T: class, constructor>(const AJSON: string): TObjectList<T>;
  public
    constructor Create(const AURL: string; const APorta: Integer = 8080; const ATimeout: Integer = 240000);
    destructor Destroy; override;

    function EnviarEmpresa(const AEmpresa: TEmpresa): Boolean;
    function EnviarContador(const AContador: TContador): Boolean;
    function EnviarClientes(const AClientes: TClientes): Boolean;
    function EnviarProdutos(const AProdutos: TProdutos): Boolean;
    function EnviarFinalizadoras(const AFinalizadoras: TFinalizadoras): Boolean;
    function EnviarUnidades(const AUnidades: TUnidades): Boolean;
    function EnviarUsuarios(const AUsuarios: TUsuarios): Boolean;

    function EnviarDAV(const ADAV: TDAV): Boolean;
    function EnviarPreVenda(const APreVenda: TPreVenda): Boolean;
    function EnviarVenda(var AVenda: TVenda): Boolean;

    function BuscarVendaID(const AIdVenda: string): TObjectList<TVenda>;
    function BuscarVendaPeriodo(const ADataInicial: TDateTime; const ADataFinal: TDateTime): TObjectList<TVenda>;

    function BuscarReducaoZ(const ADataMovimento: TDateTime): TObjectList<TReducaoZ>; overload;
    function BuscarReducaoZ(const ANumECF: Integer; const ANumCRZ: Integer): TObjectList<TReducaoZ>; overload;
    function BuscarReducaoZ(const ADataInicial: TDateTime; const ADataFinal: TDateTime): TObjectList<TReducaoZ>; overload;

    function BuscarXML(const AChaveAcesso: string): WideString;

    procedure AbrirDia(const AFundoCaixa: Double);
    procedure FecharDia;
    function UltimoCOO: string;
    function UltimoCCF: string;
    function NumeroPDV: string;

    function CancelarVenda(const ANumECF: Integer; const ANumCCF: Integer): Boolean; overload;
    function CancelarVenda(const AIdIntegracao: string): Boolean; overload;

    function CadastrarAliquotas(const Aliquotas: TAliquotasPadrao): Boolean;

    function IsServidorOnLine: Boolean;
    function StatusPDV: TStatusVenda;
    function DadosDoECFConectado: TVendaECF;
  end;

implementation

uses
  MVCFramework.Commons,
  MVCFramework.Serializer.JsonDataObjects;

{ TIntegracaoPDV }

procedure TIntegracaoPDV.ShowError(AResponse: IRESTResponse);
begin
  if AResponse.HasError then
    raise EIntegracaoPDV.CreateFmt('%d: %s', [AResponse.Error.HTTPError, AResponse.Error.ExceptionMessage])
  else
    raise EIntegracaoPDV.CreateFmt('%d: %s', [AResponse.ResponseCode, AResponse.ResponseText]);
end;

function TIntegracaoPDV.BodyAs<T>(const AJSON: string): T;
var
  Obj: TObject;
  lSerializer: IMVCSerializer;
begin
  Result := nil;

  if FSerializers.TryGetValue(TMVCMediaType.APPLICATION_JSON, lSerializer) then
  begin
    Obj := TMVCSerializerHelper.CreateObject(TClass(T).QualifiedClassName);
    try
      lSerializer.DeserializeObject(AJSON, Obj, TMVCSerializationType.stDefault, nil);
      Result := Obj as T;
    except
      on E: Exception do
      begin
        FreeAndNil(Obj);
        raise;
      end;
    end;
  end
  else
    raise EMVCDeserializationException.Create('Body ContentType n�o suportado');
end;

function TIntegracaoPDV.BodyAsListOf<T>(const AJSON: string): TObjectList<T>;
var
  List: TObjectList<T>;
  lSerializer: IMVCSerializer;
begin
  Result := nil;
  if FSerializers.TryGetValue(TMVCMediaType.APPLICATION_JSON, lSerializer) then
  begin
    List := TObjectList<T>.Create(True);
    try
      lSerializer.DeserializeCollection(AJSON, List, T, TMVCSerializationType.stDefault, nil);
      Result := List;
    except
      FreeAndNil(List);
      raise;
    end;
  end
  else
    raise EMVCException.Create('Body ContentType n�o suportado');
end;

procedure TIntegracaoPDV.CreateComponentes;
begin
  FRestClient := TRESTClient.Create(FURL, FPorta);
  FRESTClient.ConnectionTimeOut(FTimeout);
  FRESTClient.ReadTimeOut(FTimeout);
end;

procedure TIntegracaoPDV.FreeComponentes;
begin
  FRESTClient.DisposeOf;
end;

constructor TIntegracaoPDV.Create(const AURL: string; const APorta: Integer; const ATimeout: Integer);
var
  lDefaultSerializerContentType: string;
begin
  inherited Create;
  FURL     := AURL;
  FPorta   := APorta;
  FTimeout := ATimeout;

  FSerializers := TDictionary<string, IMVCSerializer>.Create;

  lDefaultSerializerContentType := BuildContentType(TMVCMediaType.APPLICATION_JSON, '');
  FSerializers.Add(lDefaultSerializerContentType, TMVCJSONDataObjectsSerializer.Create);
end;

destructor TIntegracaoPDV.Destroy;
begin
  FSerializers.Free;
  inherited;
end;

procedure TIntegracaoPDV.AbrirDia(const AFundoCaixa: Double);
var
  oFundoCaixa: TFundoCaixaModel;
begin
  CreateComponentes;
  try
    oFundoCaixa := TFundoCaixaModel.Create;
    try
      oFundoCaixa.Valor := AFundoCaixa;

      FRESTResponse := FRESTClient.Resource('/api/pdv/abrirdia').doPOST<TFundoCaixaModel>(oFundoCaixa, False);
      if not FRESTResponse.ResponseCode = 200 then
        ShowError(FRESTResponse);
    finally
      oFundoCaixa.DisposeOf;
    end;
  finally
    FreeComponentes;
  end;
end;

procedure TIntegracaoPDV.FecharDia;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.doPOST('/api/pdv/fechardia', []);
    if not FRESTResponse.ResponseCode = 200 then
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.EnviarClientes(const AClientes: TClientes): Boolean;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.Resource('/api/cliente').doPOST<TClientes>(AClientes, False);
    Result := FRESTResponse.ResponseCode = 200;
    if not Result then
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.EnviarContador(const AContador: TContador): Boolean;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.Resource('/api/contador').doPOST<TContador>(AContador, False);
    Result := FRESTResponse.ResponseCode = 200;
    if not Result then
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.EnviarEmpresa(const AEmpresa: TEmpresa): Boolean;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.Resource('/api/empresa').doPOST<TEmpresa>(AEmpresa, False);
    Result := FRESTResponse.ResponseCode = 200;
    if not Result then
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.EnviarFinalizadoras(const AFinalizadoras: TFinalizadoras): Boolean;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.Resource('/api/finalizadora').doPOST<TFinalizadoras>(AFinalizadoras, False);
    Result := FRESTResponse.ResponseCode = 200;
    if not Result then
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.EnviarDAV(const ADAV: TDAV): Boolean;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.Resource('/api/dav').doPOST<TDAV>(ADAV, False);
    Result := FRESTResponse.ResponseCode = 200;
    if not Result then
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.EnviarPreVenda(const APreVenda: TPreVenda): Boolean;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.Resource('/api/prevenda').doPOST<TPreVenda>(APreVenda, False);
    Result := FRESTResponse.ResponseCode = 200;
    if not Result then
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.EnviarProdutos(const AProdutos: TProdutos): Boolean;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.Resource('/api/produto').doPOST<TProdutos>(AProdutos, False);
    Result := FRESTResponse.ResponseCode = 200;
    if not Result then
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.EnviarUnidades(const AUnidades: TUnidades): Boolean;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.Resource('/api/unidade').doPOST<TUnidades>(AUnidades, False);
    Result := FRESTResponse.ResponseCode = 200;
    if not Result then
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.EnviarUsuarios(const AUsuarios: TUsuarios): Boolean;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.Resource('/api/usuario').doPOST<TUsuarios>(AUsuarios, False);
    Result := FRESTResponse.ResponseCode = 200;
    if not Result then
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.EnviarVenda(var AVenda: TVenda): Boolean;
var
  ErrorCode: Integer;
  ErrorMessage: string;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.Resource('/api/venda').doPOST<TVenda>(AVenda, False);

    Result := FRESTResponse.ResponseCode = 200;
    if not Result then
      ShowError(FRESTResponse);

    AVenda := BodyAs<TVenda>(FRESTResponse.BodyAsString);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.BuscarReducaoZ(const ADataInicial, ADataFinal: TDateTime): TObjectList<TReducaoZ>;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.doGET('/api/reducaoz/periodo', [
        FormatDateTime('ddmmyyyy', ADataInicial),
        FormatDateTime('ddmmyyyy', ADataFinal)
      ]);

    if FRESTResponse.ResponseCode <> 200 then
      ShowError(FRESTResponse);

    Result := BodyAsListOf<TReducaoZ>(FRESTResponse.BodyAsString);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.BuscarReducaoZ(const ANumECF, ANumCRZ: Integer): TObjectList<TReducaoZ>;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.doGET('/api/reducaoz/periodo', [ANumECF.ToString, ANumCRZ.ToString]);

    if FRESTResponse.ResponseCode <> 200 then
      ShowError(FRESTResponse);

    Result := BodyAsListOf<TReducaoZ>(FRESTResponse.BodyAsString);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.BuscarReducaoZ( const ADataMovimento: TDateTime): TObjectList<TReducaoZ>;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.doGET('/api/reducaoz/periodo', [FormatDateTime('ddmmyyyy', ADataMovimento)]);
    if FRESTResponse.ResponseCode <> 200 then
      ShowError(FRESTResponse);

    Result := BodyAsListOf<TReducaoZ>(FRESTResponse.BodyAsString);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.BuscarVendaID(const AIdVenda: string): TObjectList<TVenda>;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.doGET('/api/venda', [AIdVenda]);
    if FRESTResponse.ResponseCode <> 200 then
      ShowError(FRESTResponse);

    Result := BodyAsListOf<TVenda>(FRESTResponse.BodyAsString);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.BuscarVendaPeriodo(const ADataInicial, ADataFinal: TDateTime): TObjectList<TVenda>;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.doGET('/api/venda', [
      FormatDateTime('ddmmyyyy', ADataInicial),
      FormatDateTime('ddmmyyyy', ADataFinal)
    ]);

    if FRESTResponse.ResponseCode <> 200 then
      ShowError(FRESTResponse);

    Result := BodyAsListOf<TVenda>(FRESTResponse.BodyAsString);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.BuscarXML(const AChaveAcesso: string): WideString;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.doGET('/api/xml', [AChaveAcesso]);
    if FRESTResponse.ResponseCode <> 200 then
      ShowError(FRESTResponse);

    Result := FRESTResponse.BodyAsString;
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.CancelarVenda(const ANumECF: Integer; const ANumCCF: Integer): Boolean;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.doPOST('/api/venda/cancelar', [ANumECF.ToString, ANumCCF.ToString]);
    Result := FRESTResponse.ResponseCode = 200;
    if not Result then
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.CadastrarAliquotas(const Aliquotas: TAliquotasPadrao): Boolean;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient
      .Resource('/api/pdv/aliquotas')
      .doPOST<TAliquotaPadrao>(Aliquotas, False);

    Result := FRESTResponse.ResponseCode = 200;
    if not Result then
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.CancelarVenda(const AIdIntegracao: string): Boolean;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.doPOST('/api/venda/cancelar', [AIdIntegracao]);
    Result := FRESTResponse.ResponseCode = 200;
    if not Result then
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.IsServidorOnLine: Boolean;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.doGET('/api/server/status', []);
    Result := FRESTResponse.ResponseCode = 200;
    if not Result then
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.StatusPDV: TStatusVenda;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.doGET('/api/pdv/status', []);
    if FRESTResponse.ResponseCode = 200 then
      Result := BodyAs<TStatusVenda>(FRESTResponse.BodyAsString)
    else
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.UltimoCCF: string;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.doGET('/api/pdv/ccf', []);
    if FRESTResponse.ResponseCode = 200 then
      Result := BodyAs<TResultModel>(FRESTResponse.BodyAsString).Result
    else
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.UltimoCOO: string;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.doGET('/api/pdv/coo', []);
    if FRESTResponse.ResponseCode = 200 then
      Result := BodyAs<TResultModel>(FRESTResponse.BodyAsString).Result
    else
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.NumeroPDV: string;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.doGET('/api/pdv/numero', []);
    if FRESTResponse.ResponseCode = 200 then
      Result := BodyAs<TResultModel>(FRESTResponse.BodyAsString).Result
    else
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

function TIntegracaoPDV.DadosDoECFConectado: TVendaECF;
begin
  CreateComponentes;
  try
    FRESTResponse := FRESTClient.doGET('/api/pdv/dados', []);
    if FRESTResponse.ResponseCode = 200 then
      Result := BodyAs<TVendaECF>(FRESTResponse.BodyAsString)
    else
      ShowError(FRESTResponse);
  finally
    FreeComponentes;
  end;
end;

end.

